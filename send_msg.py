from email.message import EmailMessage
import smtplib


def send_message(email_from, email_to, subject, text, **kwargs):
    msg = EmailMessage()
    msg.set_content(text)
    mail_lib = smtplib.SMTP_SSL('smtp.yandex.ru', 465)
    if len(kwargs):
        password = kwargs['password']
        if kwargs.get('user', -1) == -1:
            user = email_from
        else:
            user = kwargs['user']
        mail_lib.login(user, password)
    msg['Subject'] = subject
    msg['From'] = email_from
    msg['To'] =  email_to
    mail_lib.login(user, password)
    mail_lib.send_message(msg)
    mail_lib.quit()


if __name__ == '__main__':
    pass