import requests

API_KEY_1 = '657bcb6d-12be-43a5-9359-b964b5576c93' #API Поиска по организациям
API_KEY_2 = 'c2264d9a-affb-4150-8d6e-aaecfc4b247a' #JavaScript API и HTTP Геокодер

def get_organizations(coordinates):
    organizations = []
    for org_name in ['Сбербанк', 'Россельхозбанк', 'ВТБ', 'Тинькофф']:
        q = 'https://search-maps.yandex.ru/v1/?text={0}'.format(org_name) + \
            '&ll={0},{1}'.format(coordinates[0], coordinates[1]) + \
            '&lang=ru_RU' + \
            '&apikey={0}'.format(API_KEY_1)
        try:
            r = requests.get(q).json()
            for f in r['features']:
                if f['properties']['name'] != '' and f['properties']['description'] != '':
                    organizations.append({'координаты': f['geometry']['coordinates'],
                                   'наименование организации': f['properties']['name'],
                                   'описание': f['properties']['description']
                                   })
        except BaseException as e:
            print(e)
    return organizations


def organization_to_str(org):
    result = "\n"
    for k, v in org.items():
        result += k + ":" + str(v) + "\n"
    return result


if __name__ == '__main__':
    pass

