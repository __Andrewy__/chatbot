import datetime
import re


def credit_sum_is_valid(credit_sum: str) -> str:
    match = re.match("^[1-9][0-9]+$", credit_sum)
    if match:
        return 'ОК'
    else:
        return "Сумма кредита должна быть целым положительным числом"


def fullname_is_valid(fullname: str) -> str:
    if type(fullname) != str:
        return 'ФИО должна быть строкой и состоять только из букв'
    else:
        for i, word in enumerate(fullname.split()):
            if i > 2:
                return 'ФИО состоит более чем из 3 слов'
            elif not word.isalpha():
                return 'ФИО должна быть строкой и состоять только из букв'
        return 'ОК'


def birthdate_is_valid(birthdate: str) -> str:
    try:
        datetime.datetime.strptime(birthdate, '%d.%m.%Y')
    except ValueError:
        return "День рождения должно иметь формат дд.мм.гггг"
    return 'ОК'


def phone_is_valid(phone: str) -> str:
    match = re.match("9[0-9]{2}-[0-9]{3}-[0-9]{2}-[0-9]{2}", phone)
    if match:
        return 'ОК'
    else:
        return "Номер телефона имеет формат xxx-xxx-xx-xx"


def email_is_valid(email: str) -> str:
    match = re.match("[^@]+@[^@]+\.[^@]+", email)
    if match:
        return 'OK'
    else:
        return "Неверный формат email"


def period_is_valid(period: str) -> str:
    match = re.match("^[1-9][0-9]*$", period)
    if match:
        return 'OK'
    else:
        return "Срок кредита должен быть целым положительным числом"


if __name__ == '__main__':

    print('-' * 30)
    print(credit_sum_is_valid('-100'))
    print(credit_sum_is_valid('100.1'))
    print(credit_sum_is_valid('100'))

    print('-' * 30)
    print(fullname_is_valid('100'))
    print(fullname_is_valid('Ануфриев  Андрей Михайлович Россельхоъбанк'))
    print(fullname_is_valid('Ануфриев  Андрей Михайлович '))

    print('-' * 30)
    print(birthdate_is_valid('x2.06.1995'))
    print(birthdate_is_valid('x2.06. 1995'))
    print(birthdate_is_valid('x2.0@.1995'))
    print(birthdate_is_valid('20.06.1995'))

    print('-' * 30)
    print(phone_is_valid('16-839-12-32'))
    print(phone_is_valid('916-839-12-asdas32'))
    print(phone_is_valid('916sad'))
    print(phone_is_valid('916-839-12-32'))

    print('-' * 30)
    print(email_is_valid('yandex.ru'))
    print(email_is_valid('alex@google.com'))
    print(email_is_valid('amirdiana@yandex.ru'))
    print(email_is_valid('andrey-libman@yandex.ru'))

    print('-' * 30)
    print(period_is_valid('0.x'))
    print(period_is_valid('0.24'))
    print(period_is_valid('1'))
    print(period_is_valid('24'))