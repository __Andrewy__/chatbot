from checking import *
from send_msg import *
from geo import *

import telebot
import credits_database
import client_database

TOKEN = '1195592926:AAGqfKXPpdpGnJR8Xkrv1QBl9ie6Gcerupg'

class Person(object):

    def __init__(self):
        self.user_id = None
        self.credit_sum = None
        self.fullname = None
        self.birthdate = None
        self.phone = None
        self.email = None
        self.period = None

    def __str__(self):
        return "=" * 25 + "\nuser_id:" + str(self.user_id) + \
               "\nсумма кредита:" + str(self.credit_sum) + " руб" \
                "\nФИО:" + str(self.fullname) + \
               "\nДата рождения:" + str(self.birthdate) + \
               "\nНомер телефона:" + str(self.phone) + \
               "\nemail:" + \
               "срок кредита:" + str(self.period) + "\n" + "=" * 25


stage_dict = {
    'credit_sum' : 'введите сумму кредита (руб)',
    'fullname' : 'введите ФИО (формат Фамилия Имя [Отчество])',
    'birthdate' : 'введите дату рождения (формат дд.мм.гггг)',
    'phone' : 'введите номер телефона (формат xxx-xxx-xx-xx)',
    'email' : 'введите почту',
    'period' : 'введите срок кредита (число месяцев)'}




credits_database.delete_table()
credits_database.create_database()

print('start')

person = Person()
stage = ''


bot = telebot.TeleBot(token=TOKEN)



@bot.message_handler(commands=['geo'])
def geo(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    button_geo = telebot.types.KeyboardButton(text="Отправить местоположение", request_location=True)
    keyboard.add(button_geo)
    bot.send_message(message.chat.id, "Для поиска ближайших банков отправьте Ваше местоположение", reply_markup=keyboard)


@bot.message_handler(content_types=["location"])
def location(message):
    if message.location is not None:
        coordinates = (message.location.longitude, message.location.latitude)
        organizations = get_organizations(coordinates)
        if organizations:
            bot.send_message(message.chat.id, "Ближайшие к Вам банки:\n" + "=" * 25 + "\n")
            for org in organizations:
                bot.send_message(message.chat.id, organization_to_str(org))
                bot.send_message(message.chat.id, "=" * 25 + "\n")
        else:
            bot.send_message(message.chat.id, "Не удалось найти ближайшие к Вам банки")
    else:
        bot.send_message(message.chat.id, "Не удалось найти ближайшие к Вам банки")


@bot.message_handler(commands=['application'])
def application(message):
    global person, stage

    person = Person()
    person.user_id = message.chat.id
    stage = 'credit_sum'
    bot.send_message(message.chat.id, stage_dict[stage])


@bot.message_handler(commands=['all_commands'])
def send_all_commands(message):
    bot.reply_to(message, "/all_commands - все команды\n" \
                      "/about - описание бота\n" \
                      "/application - заполнить заявку\n" \
                      "/cancel - отменить заполнение заявки\n"\
                        "/geo - найти ближайшие банки")


@bot.message_handler(commands=['about'])
def send_about(message):
    bot.reply_to(message, "Бот позволяет оформить заявку на потребительский кредит.\n" \
                    "Нажмите команду '/all_commands' (без кавычек), чтобы получить список всех команд.\n" \
                    "Нажмите команду '/application' (без кавычек), чтобы начать оформлять заявку.\n" \
                    "Вам нужно будет ввести о себе информацию.\n" \
                    "Все данные разделяются пробелами (например, ФИО вводится в формате 'Фамилия Имя Отчество (при наличии)').\n"\
                    "После ввода всех данных Вы можете отправить или отменить заявку.\n")


@bot.message_handler(commands=['cancel'])
def send_cancel(message):
    stage = ''


@bot.message_handler(content_types=['text'])
def send_text(message):
    global person, stage

    if stage == '':
        bot.send_message(message.chat.id, 'Я Вас не понимаю.\n'
                                          'Если хотите просмотреть все команды, введите /all_commands\n'
                                          'Если хотите заполнить заявку, введите /application')
    elif stage == 'credit_sum':
        result = credit_sum_is_valid(message.text)
        if result == 'ОК':
            person.credit_sum = message.text
            stage = 'fullname'
            bot.send_message(message.chat.id, stage_dict[stage])
        else:
            bot.send_message(message.chat.id, result + "\nВведите сумму кредита ещё раз")

    elif stage == 'fullname':
        result = fullname_is_valid(message.text)
        if result == 'ОК':
            person.fullname = message.text
            stage = 'birthdate'
            bot.send_message(message.chat.id, stage_dict[stage])
        else:
            bot.send_message(message.chat.id, result + "\nВведите ФИО ещё раз")

    elif stage == 'birthdate':
        result = birthdate_is_valid(message.text)
        if result == 'ОК':
            person.birthdate = message.text
            stage = 'phone'
            bot.send_message(message.chat.id, stage_dict[stage])
        else:
            bot.send_message(message.chat.id, result + "\nВведите дату рождения ещё раз")

    elif stage == 'phone':
        result = phone_is_valid(message.text)
        if result == 'ОК':
            person.phone = message.text
            stage = 'email'
            bot.send_message(message.chat.id, stage_dict[stage])
        else:
            bot.send_message(message.chat.id, result + "\nВведите номер телефона ещё раз")

    elif stage == 'email':
        result = email_is_valid(message.text)
        if result == 'OK':
            person.email = message.text
            stage = 'period'
            bot.send_message(message.chat.id, stage_dict[stage])
        else:
            bot.send_message(message.chat.id, result + "\nВведите email ещё раз")

    elif stage == 'period':
        result = period_is_valid(message.text)
        if result == 'OK':
            person.period = message.text
            stage = 'all_filled'
            bot.send_message(message.chat.id, 'Ваши данные:\n' +
                             "сумма кредита:{} руб\n".format(person.credit_sum) +
                             "ФИО:{}\n".format(person.fullname) +
                             "дата рождения:{}\n".format(person.birthdate) +
                             "номер телефона:8-{}\n".format(person.phone) +
                             "почта:{}\n".format(person.email) +
                             "срок кредита:{}".format(person.period))
            keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
            keyboard.row('Отправить заявку', 'Отменить заявку')
            bot.send_message(message.chat.id, 'Выберите действие', reply_markup=keyboard)
        else:
            bot.send_message(message.chat.id, result + "\nВведите срок кредита ещё раз")

    elif stage == 'all_filled':
        if message.text == 'Отправить заявку':
            client_database.add_record(person)
            bot.send_message(message.chat.id, 'Заявка отправлена')
            try:
                credits = credits_database.request(credit_sum = person.credit_sum, period = person.period)
                if credits == []:
                    bot.send_message(message.chat.id, "\nК сожалению, на данный момент нет подходящих для Вас предложений")
                else:
                    result = credits_database.request_to_str(credits)
                    try:
                        send_message(email_from='rosselhozbank.app@yandex.ru',
                                     email_to=person.email,
                                     subject='Заявка на кредит',
                                     text="Добрый день!\nВы заполнили заявку на получение кредита:\n" +
                                          str(person) + "\nНаиболее подходящие для Вас условия кредита:\n" + result,
                                     password="123321qazzaq")
                    except BaseException as e:
                        print(e)
                    bot.send_message(message.chat.id, '\nНаиболее подходящие для Вас условия кредита:\n' + result)
            except BaseException as e:
                print(e)
                bot.send_message(message.chat.id, 'Ошибка при отправке заявки. Возможно, Вы ввели неверно email')
            stage = ''
        else:
            bot.send_message(message.chat.id, 'Заявка отменена')
            stage = ''


bot.polling()

