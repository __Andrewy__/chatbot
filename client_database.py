import sqlite3


def add_record(person):
    conn = sqlite3.connect("Applications_DataBase.db")
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS Applications
                      (user_id text PRIMARY KEY, credit_sum int, fullname text,
                       birthdate text, phone text, email text, period text)
                   """)
    attributes = person.__dict__.keys()
    values = {a : person.__dict__[a] for a in attributes}
    try:
        cursor.execute("""INSERT INTO Applications VALUES (:user_id, :credit_sum, :fullname, :birthdate, :phone, :email, :period);""", values)
    except BaseException as e:
        print(e.args)
    conn.commit()
    conn.close()


def print_rows():
    try:
        conn = sqlite3.connect("Applications_DataBase.db")
        cursor = conn.cursor()
        for row in cursor.execute("SELECT * FROM Applications"):
            print(row)
        conn.close()
    except BaseException as e:
        print(e)


def delete_table():
    conn = sqlite3.connect("Applications_DataBase.db")
    cursor = conn.cursor()
    cursor.execute("""DROP TABLE IF EXISTS Applications""")
    conn.close()


if __name__ == '__main__':

    #delete_table()

    print_rows()
