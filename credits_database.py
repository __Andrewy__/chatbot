import sqlite3


def create_database(name_table = 'Credits'):
    conn = sqlite3.connect(name_table + "_DataBase.db")
    cursor = conn.cursor()
    cursor.execute('CREATE TABLE IF NOT EXISTS {0}'
                   '(credit_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, credit_sum int, period int)'.format(name_table))

    query = 'CREATE TABLE IF NOT EXISTS {0}' \
            ' (credit_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, credit_sum int, period int)'.format(name_table)
    cursor.execute(query)

    for credit_sum in range(10000, 500000, 20000):
        for period in range(3, 25):
            try:
                cursor.execute(
                    """INSERT INTO Credits VALUES (:credit_id, :credit_sum, :period);""", (None, credit_sum, period))
            except BaseException as e:
                print(e.args)
    conn.commit()
    conn.close()


def print_rows(name_table = 'Credits'):
    try:
        conn = sqlite3.connect(name_table + "_DataBase.db")
        cursor = conn.cursor()
        for row in cursor.execute("SELECT * FROM {0}".format(name_table)):
            print(row)
        conn.close()
    except BaseException as e:
        print(e)


def delete_table(name_table = 'Credits'):
    conn = sqlite3.connect(name_table + "_DataBase.db")
    cursor = conn.cursor()
    cursor.execute('DROP TABLE IF EXISTS {0}'.format(name_table))
    conn.close()


def request(credit_sum, period, name_table = 'Credits'):
    ans = []
    try:
        conn = sqlite3.connect(name_table + "_DataBase.db")
        cursor = conn.cursor()

        query = "SELECT * FROM {0} " \
                "WHERE {0}.credit_sum between {1} and {2} and {0}.period = {3}" \
                "".format(name_table, int(credit_sum) - 100000, int(credit_sum) + 100000, int(period))
        ans = []
        for row in cursor.execute(query):
            ans.append(row)
        conn.close()
        return ans
    except BaseException as e:
        print(e)
        return ans


def request_to_str(rows):
    if rows == []:
        return "="*25 + "Не найдено подходящих для Вас кредитов\n" + "="*25
    else:
        result = ""
        for row in rows:
            result += "ID кредита: {0}, сумма кредита: {1} руб, период: {2} мес\n".format(row[0], row[1], row[2])
        return result

if __name__ == '__main__':

    #print_rows()
    pass